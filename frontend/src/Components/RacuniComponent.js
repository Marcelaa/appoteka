import React, {Component} from "react";
import RacuniReactService from "../ReactService/RacuniReactService";

class RacuniComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            racuni: [  ]
                /*
                {idDokumenta: 1, datumDokumenta: '05-05-2022', vrijemeRacuna: '15:49', nacinPlacanja:'gotovina', iznosRacuna:'329',
                stavkeDokumenta: [{kolicinaLijekaDokumenta: 2, lijek: {idLijeka:1, imeLijeka: "Aspirin", vrstaLijeka: "antipiretik", cijena: "32.00", kolicinaNaZalihi:"27", proizvodac:"Bayer d.o.o." }},
                    {kolicinaLijekaDokumenta: 5, lijek: {idLijeka:2, imeLijeka: "Brufen", vrstaLijeka: "analgetik", cijena: "45.00", kolicinaNaZalihi:"53", proizvodac:"Mylan d.o.o." }}]},
                {idDokumenta: 2, datumDokumenta: '06-05-2022', vrijemeRacuna: '12:02', nacinPlacanja:'kartica', iznosRacuna:'122',
                    stavkeDokumenta: [{kolicinaLijekaDokumenta: 1, lijek: {idLijeka:1, imeLijeka: "Aspirin", vrstaLijeka: "antipiretik", cijena: "32.00", kolicinaNaZalihi:"27", proizvodac:"Bayer d.o.o." }},
                        {kolicinaLijekaDokumenta: 2, lijek: {idLijeka:2, imeLijeka: "Brufen", vrstaLijeka: "analgetik", cijena: "45.00", kolicinaNaZalihi:"53", proizvodac:"Mylan d.o.o." }}]}
            */

        }

        this.detaljiOLijeku = this.detaljiOLijeku.bind(this);
    }

    async componentDidMount() {
        await RacuniReactService.getRacuni().then((res) => {
            this.setState({racuni: res.data})
        })
    }

    detaljiOLijeku(idLijeka) {
        window.sessionStorage.setItem('idLijeka', idLijeka);
        window.location.replace('/detaljiolijeku');
    }

    render() {

        return (
            <>
                <section id="about" className="center-element">
                    <a href="/noviracun">
                        <figure >
                            <span><i className="fa fa-plus"></i></span>
                            <figcaption>
                                <h3 className="center-text">Dodaj novi račun!</h3>
                            </figcaption>
                        </figure>
                    </a>
                </section>
                <section id="about" className="element-min-pad">
                    <div className="container">
                        <h2>Popis računa:</h2>
                        <div className="row">
                            {
                                this.state.racuni.map(
                                    racun=>
                                        <div key={racun.idDokumenta}>
                                            <div className="col-md-6 col-sm-12">
                                                <div className="about-info">
                                                    <h2>Stavke računa:</h2>
                                                    {racun.stavkeDokumenta.map(
                                                        stavkaRacuna=>
                                                            <figure key={stavkaRacuna.idLijeka}>
                                                                <span><i className="fa fa-ellipsis-v"></i></span>
                                                                <figcaption>
                                                                    <h3>{stavkaRacuna.lijek.imeLijeka}</h3>
                                                                    <p className="full-width">Količina: {stavkaRacuna.kolicinaLijekaDokumenta} kom., Cijena: {stavkaRacuna.lijek.cijenaLijeka} kn <a className="btn  btn-space " onClick={() => this.detaljiOLijeku(stavkaRacuna.lijek.idLijeka)}>Više o lijeku</a></p>
                                                                </figcaption>
                                                            </figure>
                                                    )}

                                                </div>
                                            </div>

                                            <div className="col-md-offset-1 col-md-4 col-sm-12">
                                                <div className="entry-form">
                                                        <h2>Račun # {racun.idDokumenta}</h2>
                                                        <h2>{racun.datumDokumenta} {racun.vrijemeRacuna}</h2>
                                                        <h2>Način plaćanja: {racun.nacinPlacanja}</h2>
                                                        <h2>Iznos računa: {racun.iznosRacuna} kn</h2>
                                                        <a href="#feature" className="section-btn btn btn-default smoothScroll"> Uredi</a>
                                                        <a href="#feature" className="section-btn btn btn-default smoothScroll"> Izbriši</a>

                                                </div>
                                            </div>

                                        </div>

                                )
                            }
                        </div>
                    </div>
                </section>
            </>
        );
    }
};

export default RacuniComponent;