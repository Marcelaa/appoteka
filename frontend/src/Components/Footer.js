import React from "react";

const Footer = () => {

    return (
        <>
            <footer id="footer">
                <div className="container">
                    <div className="row">

                        <div className="col-md-4 col-sm-6">
                            <div className="footer-info">
                                <div className="section-title">
                                    <h2>Appoteka</h2>
                                </div>
                                <address>
                                    <p>Unska ulica 3,<br/> 10000 Zagreb, Hrvatska</p>
                                </address>

                                <ul className="social-icon">
                                    <li><a href="#" className="fa fa-facebook-square" attr="facebook icon"></a></li>
                                    <li><a href="#" className="fa fa-twitter"></a></li>
                                    <li><a href="#" className="fa fa-instagram"></a></li>
                                </ul>

                                <div className="copyright-text">
                                    <p>Copyright &copy; 2022 Appoteka</p>

                                    <p>Design: TemplateMo</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4 col-sm-6">
                            <div className="footer-info">
                                <div className="section-title">
                                    <h2>Kontakt</h2>
                                </div>
                                <address>
                                    <p>+385 01 444 444, +385 01 444 442</p>
                                    <p><a href="mailto:youremail.co">info@appoteka.hr</a></p>
                                </address>

                                <div className="footer_menu">
                                    <h2>Linkovi</h2>
                                    <ul>
                                        <li><a href="/">Naslovnica</a></li>
                                        <li><a href="/lijekovi">Lijekovi</a></li>
                                        <li><a href="/racuni">Računi</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4 col-sm-12">

                        </div>

                    </div>
                </div>
            </footer>
        </>
    );

};

export default Footer;