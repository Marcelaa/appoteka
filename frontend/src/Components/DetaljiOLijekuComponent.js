import React, {Component} from "react";
import LijekoviReactService from "../ReactService/LijekoviReactService";

class DetaljiOLijekuComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idLijeka: '',
            imeLijeka: '',
            vrstaLijeka: '',
            cijena: '',
            kolicinaNaZalihi:'',
            proizvodac: '',
            bezreceptniLijek: ''
        }

        this.editLijek = this.editLijek.bind(this);
        this.deleteLijek = this.deleteLijek.bind(this);
    }

    async componentDidMount(){
        if(window.sessionStorage.getItem('idLijeka')) {
            await LijekoviReactService.getLijekById(window.sessionStorage.getItem('idLijeka')).then((res) => {
                this.setState({idLijeka: res.data.idLijeka })
                this.setState({imeLijeka: res.data.imeLijeka})
                this.setState({vrstaLijeka: res.data.vrstaLijeka})
                this.setState({cijena: res.data.cijenaLijeka })
                this.setState({kolicinaNaZalihi: res.data.kolicinaNaZalihi})
                this.setState({proizvodac: res.data.proizvodac})
                this.setState({bezreceptniLijek: res.data.bezreceptniLijek})
            })
        }
    }

    deleteLijek(idLijeka){
        /*
        ReactBiljeskaService.deleteBiljeska(idBiljekse).then( res => {
            this.setState({biljeske: this.state.biljeske.filter(biljeska => biljeska.idBiljeske !== idBiljekse)});
        });
        */
    }

    editLijek(idLijeka){
        window.sessionStorage.setItem('idLijeka', idLijeka);
        window.location.replace('/uredilijek')
    }

    render() {

        return (
            <>
                <section id="feature" className="element-min-pad">
                    <div className="container">
                        <div className="container">
                            <h2>Detalji o lijeku:</h2>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-4">
                                <div className="feature-thumb">
                                    <span>{this.state.idLijeka}</span>
                                    <h3>{this.state.imeLijeka}</h3>
                                    <p>Vrsta: {this.state.vrstaLijeka}</p>
                                    <p>Bezreceptni: {this.state.bezreceptniLijek}</p>
                                    <p>Količina na zalihi: {this.state.kolicinaNaZalihi}</p>
                                    <p>Proizvođač: {this.state.proizvodac.nazivProizvodaca}</p>
                                    <p>Cijena: {this.state.cijena} kn</p>
                                    <a href="#feature" className="section-btn btn btn-default smoothScroll green" onClick={() => this.editLijek(this.state.idLijeka)}> Uredi</a>
                                    <a href="#feature" className="section-btn btn btn-default smoothScroll green" onClick={() => this.deleteLijek(this.state.idLijeka)}> Izbriši</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
};

export default DetaljiOLijekuComponent;