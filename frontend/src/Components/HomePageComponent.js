import React, {Component} from "react";

class HomePageComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <>
                    <div className="row">
                        <div className="owl-theme home-slider">
                            <div className="item item-first">
                                <div className="caption">
                                    <div className="container">
                                        <div className="col-md-6 col-sm-12">
                                            <h1>Appoteka</h1>
                                            <h3>Vaša e-lijekarna!</h3>
                                            <a href="/lijekovi" className="section-btn btn btn-default smoothScroll">Pregledaj lijekove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            </>
        );
    }
};

export default HomePageComponent;