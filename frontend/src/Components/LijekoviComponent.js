import React, {Component} from "react";
import LijekoviReactService from "../ReactService/LijekoviReactService";

class LijekoviComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lijekovi: []
            /*
                [{idLijeka:1, imeLijeka: "Aspirin", vrstaLijeka: "antipiretik", cijena: "32.00", kolicinaNaZalihi:"27", proizvodac:"Bayer d.o.o." },
                {idLijeka:2, imeLijeka: "Brufen", vrstaLijeka: "analgetik", cijena: "45.00", kolicinaNaZalihi:"53", proizvodac:"Mylan d.o.o." }] //[]
        */
        }


        this.editLijek = this.editLijek.bind(this);
        this.deleteLijek = this.deleteLijek.bind(this);
    }

    async componentDidMount() {
        await LijekoviReactService.getLijekovi().then((res) => {
            this.setState({lijekovi: res.data})
        })
    }

    deleteLijek(idLijeka){
        console.log(idLijeka)
        LijekoviReactService.deleteLijek(idLijeka).then( res => {
            this.setState({lijekovi: this.state.lijekovi.filter(lijek => lijek.idLijeka !== idLijeka)});
        });
    }

    editLijek(idLijeka){
        window.sessionStorage.setItem('idLijeka', idLijeka);
        window.location.replace('/uredilijek');
    }

    render() {

        return (
            <>
                <section id="about" className="center-element element-min-pad">
                        <figure >
                            <a href="/novilijek">
                            <span><i className="fa fa-plus"></i></span>
                            <figcaption>
                                <h3 className="center-text">Dodaj novi lijek!</h3>
                            </figcaption>
                            </a>
                        </figure>
                </section>
                <section id="feature" className="element-min-pad">
                    <div className="container">
                        <div className="container">
                            <h2>Popis lijekova:</h2>
                        </div>
                        <div className="row">
                            {
                                this.state.lijekovi.map(
                                    lijek =>
                                <div className="col-md-4 col-sm-4" key={lijek.idLijeka}>
                                    <div className="feature-thumb">
                                        <span>{lijek.idLijeka}</span>
                                        <h3>{lijek.imeLijeka}</h3>
                                        <p>Vrsta: {lijek.vrstaLijeka}</p>
                                        <p>Količina na zalihi: {lijek.kolicinaNaZalihi}</p>
                                        <p>Proizvođač: {lijek.proizvodac.nazivProizvodaca}</p>
                                        <p>Cijena: {lijek.cijenaLijeka} kn</p>
                                        <a href="#feature" className="section-btn btn btn-default smoothScroll green" onClick={() => this.editLijek(lijek.idLijeka)}> Uredi</a>
                                        <a href="#feature" className="section-btn btn btn-default smoothScroll green" onClick={() => this.deleteLijek(lijek.idLijeka)}> Izbriši</a>
                                    </div>
                                </div>
                                )
                            }
                        </div>
                    </div>
                </section>
            </>
        );
    }
};

export default LijekoviComponent;