import React, {Component} from "react";

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {

    return (
        <>
            <section className="navbar custom-navbar navbar-fixed-top" role="navigation">
                <div className="container">

                    <div className="navbar-header">
                        <button className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span className="icon icon-bar"></span>
                            <span className="icon icon-bar"></span>
                            <span className="icon icon-bar"></span>
                        </button>

                        <a href="#" className="navbar-brand">Appoteka</a>
                    </div>

                    <div className="collapse navbar-collapse">
                        <ul className="nav navbar-nav navbar-nav-first">
                            <li><a href="/" className="smoothScroll">Naslovnica</a></li>
                            <li><a href="/lijekovi" className="smoothScroll">Lijekovi</a></li>
                            <li><a href="/racuni" className="smoothScroll">Računi</a></li>
                        </ul>

                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#"><i className="fa fa-phone"></i> +385 01 444 444</a></li>
                        </ul>
                    </div>

                </div>
            </section>
        </>
    );
    }
};

export default Header;