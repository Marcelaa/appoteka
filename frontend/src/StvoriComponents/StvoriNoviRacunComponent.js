import React, {Component} from "react";
import Select from 'react-select'
import RacuniReactService from "../ReactService/RacuniReactService";
import LijekoviReactService from "../ReactService/LijekoviReactService";

class StvoriNoviRacunComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            /* datum i vrijeme dokumenta-> automatski */
            nacinPlacanja:'',
            iznosRacuna: 0,
            stavkeDokumenta:[],
            opcijePlacanja: ['gotovina', 'kartica'],
            kolicinaNoveStavke: 1,
            lijekNoveStavke: '',
            dohvaceniLijekovi: [],
            izborLijekova: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeLijeka = this.handleChangeLijeka.bind(this);
        this.handleChangeNacinaPlacanja = this.handleChangeNacinaPlacanja.bind(this);
        this.handleChangeStavke = this.handleChangeStavke.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.saveNewRacun = this.saveNewRacun.bind(this);
        this.addStavkaRacuna = this.addStavkaRacuna.bind(this);
        this.removeStavkaRacuna = this.removeStavkaRacuna.bind(this);
    }


    async componentDidMount(){
        await LijekoviReactService.getLijekovi().then((res) => {
            this.setState({dohvaceniLijekovi: res.data})
        })
        let poljeLijekova = [];
        this.state.dohvaceniLijekovi.map( lijek=> {
                let labelaLijeka = lijek.imeLijeka + " (" + lijek.vrstaLijeka + "), " + lijek.proizvodac + ", " + lijek.cijenaLijeka + " kn"
                poljeLijekova.push({label:labelaLijeka, value:lijek})
            }
        )
        this.setState({izborLijekova: poljeLijekova});
    }

    handleChangeLijeka(e) {
        this.setState({ lijekNoveStavke: e.value });
    }

    async handleChangeStavke(e) {
        await this.setState({stavkeDokumenta: '' })
        e.map(stavka => (
            this.setState({stavkeDokumenta: this.state.stavkeDokumenta + "," + "kolicinaLijekaDokumenta:" + stavka.kolicina + ", " + "lijek: " + stavka.lijek})
        ))
        let suma = 0;

        this.state.stavkeDokumenta.map( stavka=>{
            suma += (stavka.lijekNoveStavke.cijena)
        })
    }

    addStavkaRacuna() {
        let noveStavke;
        if(this.state.lijekNoveStavke.length != 0) {
            noveStavke = this.state.stavkeDokumenta
            noveStavke.push({ kolicinaLijekaDokumenta: this.state.kolicinaNoveStavke, lijek: this.state.lijekNoveStavke })
            this.setState({stavkeDokumenta: noveStavke});
        }
        let suma = 0;
        noveStavke.map( stavka=> {
            suma += (stavka.lijek.cijena) * (stavka.kolicinaLijekaDokumenta)
        })
        this.setState({iznosRacuna: suma});
    }

    removeStavkaRacuna(idLijeka) {
        this.setState({stavkeDokumenta: this.state.stavkeDokumenta.filter(stavka => stavka.lijek.idLijeka !== idLijeka)});

    }

    handleChange(event) {
        this.setState({value: event.target.value});
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    saveNewRacun = (e) => {
        e.preventDefault();
        let racun = {
            nacinPlacanja: this.state.nacinPlacanja,
            iznosRacuna: this.state.iznosRacuna,
            stavkeDokumenta:this.state.stavkeDokumenta
        };

        RacuniReactService.createRacun(racun);
        window.location.replace('/racuni')

    }

    handleChangeNacinaPlacanja(e) {
        this.setState({ nacinPlacanja: e.target.value });
    }

    cancel(){
        window.location.replace('/racuni')
    }

    render() {
        const opcijePlacanja = [
            {
                label: "gotovina",
                value: "gotovina",
            },
            {
                label: "kartica",
                value: "kartica",
            }];

        return(
            <>
                <section id="about">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="about-info">
                                    <h2>Stavke računa:</h2>
                                    {this.state.stavkeDokumenta.map(
                                        stavkaRacuna=>
                                            <figure key={stavkaRacuna.idLijeka}>
                                                <span onClick={() => this.removeStavkaRacuna(stavkaRacuna.lijek.idLijeka)}><i className="fa fa-minus"></i></span>
                                                <figcaption>
                                                    <h3>{stavkaRacuna.lijek.imeLijeka}</h3>
                                                    <p className="full-width">Količina: {stavkaRacuna.kolicinaLijekaDokumenta} kom., Cijena: {stavkaRacuna.lijek.cijena} kn <a className="btn  btn-space">Više o lijeku</a></p>
                                                </figcaption>
                                            </figure>
                                    )}
                                    <figure>
                                        <a onClick={this.addStavkaRacuna}><span><i className="fa fa-plus"></i></span></a>
                                        <figcaption className="center-text">
                                            <p className="full-width"> Količina:
                                                <input type="number" name="kolicinaNoveStavke" className="" placeholder="Količina" required="" value={this.state.kolicinaNoveStavke} onChange={this.handleChange}  />
                                            </p>
                                            <p className="full-width"> Lijek: </p>
                                            <Select onChange={this.handleChangeLijeka} options={this.state.izborLijekova}>
                                            </Select>
                                        </figcaption>
                                    </figure>

                                </div>
                            </div>
                            <div className="col-md-offset-1 col-md-4 col-sm-12">
                                <div className="entry-form">
                                    <form onSubmit={this.handleSubmit}>
                                        <h2>Stvorite novi račun!</h2>
                                        <p>Način plaćanja:</p>
                                        <select onChange={this.handleChangeNacinaPlacanja}>
                                            {opcijePlacanja.map((option) => (
                                                <option
                                                    value={option.label}>  {option.label} </option>
                                            ))}
                                        </select>
                                        <p>Iznos računa: {this.state.iznosRacuna} kn</p>
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.saveNewRacun}> Stvori! </button>
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.cancel}> Odustani </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
};

export default StvoriNoviRacunComponent;