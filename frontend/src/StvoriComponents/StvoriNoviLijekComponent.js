import React, {Component} from "react";
import LijekoviReactService from "../ReactService/LijekoviReactService";
import Select from "react-select";

class StvoriNoviLijekComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            imeLijeka: '' ,
            vrstaLijeka:'',
            cijena: '',
            kolicinaNaZalihi: '',
            proizvodac:'',
            popisProizvodaca: [],
            opcijeProizvodaca: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeProizvodaca = this.handleChangeProizvodaca.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.saveNewLijek = this.saveNewLijek.bind(this);
    }

    async componentDidMount() {
        await LijekoviReactService.getProizvodaci().then((res) => {
            this.setState({popisProizvodaca: res.data})
        })
        let poljeProizvodaca = [];
        this.state.popisProizvodaca.map( proizvodac => {
                let labelaProizvodaca = proizvodac.nazivProizvodaca + ", email: " + proizvodac.emailProizvodaca;
                poljeProizvodaca.push({label:labelaProizvodaca, value:proizvodac})
            }
        )
        this.setState({opcijeProizvodaca: poljeProizvodaca});
    }

    handleChangeProizvodaca(e) {
        this.setState({ proizvodac: e.value });
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    saveNewLijek = (e) => {
        e.preventDefault();
        let lijek = {
            imeLijeka: this.state.imeLijeka,
            vrstaLijeka: this.state.vrstaLijeka,
            cijenaLijeka: this.state.cijena,
            kolicinaNaZalihi: this.state.kolicinaNaZalihi,
            proizvodac: this.state.proizvodac,
            bezreceptniLijek: 'da'
        };

        LijekoviReactService.createLijek(lijek);
        //window.location.replace('/lijekovi');
    }

    cancel(){
        //this.props.history.push('/lijekovi');
        window.location.replace('/lijekovi')
    }

    render() {
        return(
            <>
                <section id="about">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-offset-1 col-md-4 col-sm-12">
                                <div className="entry-form-big">
                                    <form onSubmit={this.handleSubmit}>
                                        <h2>Stvorite novi lijek</h2>
                                        <p>Ime:</p><input type="text" name="imeLijeka" className="form-control" placeholder="Ime lijeka" required="" value={this.state.imeLijeka} onChange={this.handleChange} />
                                        <p>Vrsta:</p><input type="text" name="vrstaLijeka" className="form-control" placeholder="Vrsta lijeka" required="" value={this.state.vrstaLijeka} onChange={this.handleChange} />
                                        <p>Cijena:</p><input type="number" name="cijena" className="form-control" placeholder="Cijena" required="" value={this.state.cijena} onChange={this.handleChange} />
                                        <p>Zaliha:</p><input type="number" name="kolicinaNaZalihi" className="form-control" placeholder="Količina na zalihi" required="" value={this.state.kolicinaNaZalihi} onChange={this.handleChange}  />
                                        <p>Proizvođač:</p>
                                        <Select onChange={this.handleChangeProizvodaca} options={this.state.opcijeProizvodaca}>
                                        </Select>
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.saveNewLijek}> Stvori! </button>
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.cancel}> Odustani </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
};

export default StvoriNoviLijekComponent;