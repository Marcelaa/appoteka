import axios from "axios";
import React from 'react';

const LIJEKOVI_REST_API_URL = 'http://localhost:8080/api/lijekovi';
const PROIZVODACI_REST_API_URL = 'http://localhost:8080/api/proizvodaci';

class LijekoviReactService {

    getLijekovi() {
        return axios.get(LIJEKOVI_REST_API_URL);
    }

    getProizvodaci() {
        return axios.get(PROIZVODACI_REST_API_URL);
    }

    getLijekById(idLijeka) {
        return axios.get(LIJEKOVI_REST_API_URL + '/' + idLijeka);
    }

    deleteLijek(idLijeka){
        return axios.delete(LIJEKOVI_REST_API_URL + '/' + idLijeka);
    }

    createLijek(lijek){
        return axios.post(LIJEKOVI_REST_API_URL, lijek ,
            {
                headers: {
                    authorization: 'Access-Control-Allow-Origin' ,
                    'Content-Type': 'application/json'
                }
            });
    }

    updateLijek(lijek, idLijeka){
        return axios.put(LIJEKOVI_REST_API_URL + "/update/" + idLijeka, lijek ,
            {
                headers: {
                    authorization: 'Access-Control-Allow-Origin' ,
                    'Content-Type': 'application/json'
                }
            });
    }

}

export default new LijekoviReactService();