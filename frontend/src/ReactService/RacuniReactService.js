import axios from "axios";
import React from 'react';

const RACUNI_REST_API_URL = 'http://localhost:8080/api/racuni';

class RacuniReactService {

    getRacuni() {
        return axios.get(RACUNI_REST_API_URL + '/details');
    }

    getRacunById(idRacuna) {
        return axios.get(RACUNI_REST_API_URL + '/' + idRacuna);
    }

    deleteRacun(idRacuna){
        return axios.delete(RACUNI_REST_API_URL + '/' + idRacuna);
    }

    createRacun(racun){
        return axios.post(RACUNI_REST_API_URL, racun ,
            {
                headers: {
                    authorization: 'Access-Control-Allow-Origin' ,
                    'Content-Type': 'application/json'
                }
            });
    }

    updateRacun(racun, idRacuna){
        return axios.put(RACUNI_REST_API_URL + "/update/" + idRacuna, racun ,
            {
                headers: {
                    authorization: 'Access-Control-Allow-Origin' ,
                    'Content-Type': 'application/json'
                }
            });
    }

}

export default new RacuniReactService();