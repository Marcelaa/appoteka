import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
//import "./css/bootstrap.min.css";
import "./css/bootstrap.min.css";
import "./css/font-awesome.min.css";
import "./css/owl.carousel.css";
import "./css/owl.theme.default.min.css";
import "./css/templatemo-style.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Routes} from 'react-router-dom';
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import HomePageComponent from "./Components/HomePageComponent";
import LijekoviComponent from "./Components/LijekoviComponent";
import RacuniComponent from "./Components/RacuniComponent";
import StvoriNoviLijekComponent from "./StvoriComponents/StvoriNoviLijekComponent";
import StvoriNoviRacunComponent from "./StvoriComponents/StvoriNoviRacunComponent";
import UrediLijekComponent from "./UrediComponents/UrediLijekComponent";
import DetaljiOLijekuComponent from "./Components/DetaljiOLijekuComponent";


function setState(state) {
  this.setState(state);
}

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (<>
            <Header/>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<HomePageComponent/>} />
                    <Route path="/lijekovi" element={<LijekoviComponent/>} />
                    <Route path="/racuni" element={<RacuniComponent/>} />
                    <Route path="/novilijek" element={<StvoriNoviLijekComponent/>} />
                    <Route path="/noviracun" element={<StvoriNoviRacunComponent/>} />
                    <Route path="/uredilijek" element={<UrediLijekComponent/>} />
                    <Route path="/detaljiolijeku" element={<DetaljiOLijekuComponent/>} />
              </Routes>
            </BrowserRouter>
            <Footer/>
        </>
    );

  }

}

export default App;
