import React, {Component} from "react";

class UrediLijekComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            imeLijeka: '' ,
            vrstaLijeka:'',
            cijena: '',
            kolicinaNaZalihi: '',
            proizvodac:''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.editLijek = this.editLijek.bind(this);
    }

    async componentDidMount(){
        if(window.sessionStorage.getItem('idLijeka')) {
            console.log(window.sessionStorage.getItem('idLijeka'));
            /*
            await ReactBiljeskaService.getBiljeskaById(window.sessionStorage.getItem('idBiljeske')).then((res) => {
                this.setState({ucenikBiljeske: res.data.ucenik.imeKorisnika + ' ' + res.data.ucenik.prezimeKorisnika})
                this.setState({razredBiljeske: res.data.razred.imeRazreda})
                this.setState({tekstBiljeske: res.data.tekstBiljeske})
            })
            */
        }
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    editLijek = (e) => {
        e.preventDefault();
        let lijek = {
            imeLijeka: this.state.imeLijeka,
            vrstaLijeka: this.state.vrstaLijeka,
            cijena: this.state.cijena,
            kolicinaNaZalihi: this.state.kolicinaNaZalihi,
            proizvodac: this.state.proizvodac
        };

        /*
        let biljeska = {
            tekstBiljeske: this.state.tekstBiljeske,
            ucenik: this.state.ucenik,
            ucitelj: this.state.ucitelj,
            razred: this.state.razred
        };

        ReactBiljeskaService.createBiljeska(biljeska)
        window.location.replace('/biljeske')
        */
    }

    cancel(){
        //this.props.history.push('/lijekovi');
        window.location.replace('/lijekovi')
    }

    render() {
        return(
            <>
                <section id="about">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-offset-1 col-md-4 col-sm-12">
                                <div className="entry-form-big">
                                    <form onSubmit={this.handleSubmit}>
                                        <h2>Uredite zapis o lijeku</h2>
                                        <p>Ime:</p><input type="text" name="imeLijeka" className="form-control" placeholder="Ime lijeka" required="" value={this.state.imeLijeka} onChange={this.handleChange} />
                                        <p>Vrsta:</p><input type="text" name="vrstaLijeka" className="form-control" placeholder="Vrsta lijeka" required="" value={this.state.vrstaLijeka} onChange={this.handleChange} />
                                        <p>Cijena:</p><input type="number" name="cijena" className="form-control" placeholder="Cijena" required="" value={this.state.cijena} onChange={this.handleChange} />
                                        <p>Zaliha:</p><input type="number" name="kolicinaNaZalihi" className="form-control" placeholder="Količina na zalihi" required="" value={this.state.kolicinaNaZalihi} onChange={this.handleChange}  />
                                        <p>Proizvođač:</p><input type="text" name="proizvodac" className="form-control" placeholder="Proizvođač lijeka" required="" value={this.state.proizvodac} onChange={this.handleChange}  />
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.editLijek}> Spremi promjene </button>
                                        <button className="submit-btn form-control" id="form-submit" type="submit" onClick={this.cancel}> Odustani </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>npm
            </>
        );
    }
};

export default UrediLijekComponent;