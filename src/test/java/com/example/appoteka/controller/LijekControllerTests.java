package com.example.appoteka.controller;

import com.example.appoteka.entity.Lijek;
import com.example.appoteka.entity.Proizvodac;
import com.example.appoteka.form.LijekForm;
import com.example.appoteka.mapper.LijekMapper;
import com.example.appoteka.repository.LijekRepository;
import com.example.appoteka.service.LijekService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LijekControllerTests {

    @Autowired
    private LijekService lijekService;

    @Autowired
    private LijekController lijekController;

    @MockBean
    private LijekMapper lijekMapper;

    @MockBean
    private LijekRepository lijekRepository;

    @Test
    public void getLijekoviTest(){
        Proizvodac proizvodac = new Proizvodac(1l, "PROIZVODAC1", "proizvodac1@gmail.com", new ArrayList<>());
        when(lijekRepository.findAll()).thenReturn(
                Stream.of(
                        new Lijek(1L, "IME_LIJEKA", 10, 20.50, "da", 2L, proizvodac ),
                        new Lijek(1L, "TEST", 20, 20.50, "ne", 1L, proizvodac)
                )
                        .collect(Collectors.toList())
        );
        assertEquals(2, lijekController.getAllLijekovi().size());
    }

    @Test
    public void getLijekByIdTest(){
        Long lijekId = 1L;
        Proizvodac proizvodac = new Proizvodac(1l, "PROIZVODAC1", "proizvodac1@gmail.com", new ArrayList<>());

        when(lijekRepository.findById(lijekId)).thenReturn(
                Optional.of(new Lijek(1L, "Betrion", 10, 20.50, "da", 2L, proizvodac)));
        assertEquals("Betrion", lijekService.getLijekById(lijekId).getImeLijeka());
        assertEquals(1L, lijekService.getLijekById(lijekId).getIdLijeka());
        assertEquals(20.50, lijekService.getLijekById(lijekId).getCijenaLijeka());
        assertEquals("da", lijekService.getLijekById(lijekId).getBezreceptniLijek());
        assertEquals("PROIZVODAC1", lijekService.getLijekById(lijekId).getProizvodac().getNazivProizvodaca());
    }

    @Test
    public void createLijekTest(){
        LijekForm lijekForm = new LijekForm("Betrion", 10, 20.50, "da", "Pliva d.o.o.");

        when(lijekMapper.mapToModel(lijekForm)).thenReturn(new Lijek());

        lijekController.createLijek(lijekForm);
        verify(lijekMapper, times(1)).mapToModel(lijekForm);
    }

    @Test
    public void editLijekTest(){
        Long lijekId = 1L;
        LijekForm lijekForm = new LijekForm("Betrion", 10, 20.50, "da", "Pliva d.o.o.");

        when(lijekRepository.existsById(lijekId)).thenReturn(true);
        when(lijekMapper.mapToModel(lijekForm)).thenReturn(new Lijek());

        lijekController.editLijek(lijekForm, lijekId);
        verify(lijekMapper, times(1)).mapToModel(lijekForm);
    }

    @Test
    public void deleteLijekByIdTest() {
        Long lijekId = 1L;
        when(lijekRepository.existsById(lijekId)).thenReturn(true);
        lijekController.deleteLijekById(lijekId);
        verify(lijekRepository, times(1)).deleteById(lijekId);
    }
}
