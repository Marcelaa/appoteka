package com.example.appoteka.controller;

import com.example.appoteka.entity.Dokument;
import com.example.appoteka.entity.Lijek;
import com.example.appoteka.entity.NacinPlacanja;
import com.example.appoteka.entity.Racun;
import com.example.appoteka.mapper.RacunMapper;
import com.example.appoteka.repository.RacunRepository;
import com.example.appoteka.service.RacunService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RacunControllerTests {

    @Autowired
    private RacunController racunController;

    @Autowired
    private RacunService racunService;

    @MockBean
    private RacunRepository repository;

    @Autowired
    private RacunMapper racunMapper;

  /*  @Test
    public void getAllRacuni(){
        Dokument dokument = new Dokument(1L, LocalDate.now(), null, new ArrayList<>());
        NacinPlacanja nacinPlacanja = new NacinPlacanja(1L, "GOTOVINA");
        when(repository.findAll()).thenReturn(
                Stream.of(
                                new Racun(1L, LocalDate.now(), 1L, dokument, nacinPlacanja),
                                new Racun(2L, LocalDate.now(), 2L, dokument, nacinPlacanja)
                        )
                        .collect(Collectors.toList())
        );
        assertEquals(2, racunController.getAllRacuni().size());
    }*/
}
