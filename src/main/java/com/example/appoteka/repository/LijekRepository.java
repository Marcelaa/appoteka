package com.example.appoteka.repository;

import com.example.appoteka.entity.Lijek;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LijekRepository extends JpaRepository<Lijek, Long> {

    Boolean existsByImeLijeka(String imeLijeka);

    Lijek findByImeLijeka(String imeLijeka);
}
