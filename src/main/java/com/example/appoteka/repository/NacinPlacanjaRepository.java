package com.example.appoteka.repository;

import com.example.appoteka.entity.NacinPlacanja;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NacinPlacanjaRepository extends JpaRepository<NacinPlacanja, Long> {

    NacinPlacanja findByNazivNacinaPlacanja(String nazivNacinaPlacanja);

    Boolean existsByNazivNacinaPlacanja(String nazivNacinaPlacanja);
}
