package com.example.appoteka.repository;

import com.example.appoteka.entity.Proizvodac;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProizvodacRepository extends JpaRepository<Proizvodac, Long> {

    Proizvodac findByNazivProizvodaca(String nazivProizvodaca);

    Boolean existsByNazivProizvodaca(String nazivProizvodaca);
}
