package com.example.appoteka.repository;

import com.example.appoteka.entity.StavkaDokumentId;
import com.example.appoteka.entity.StavkaDokumenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StavkaDokumentaRepository extends JpaRepository<StavkaDokumenta, StavkaDokumentId> {
}
