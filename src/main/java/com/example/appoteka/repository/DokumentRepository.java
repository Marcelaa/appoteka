package com.example.appoteka.repository;

import com.example.appoteka.entity.Dokument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DokumentRepository extends JpaRepository<Dokument, Long> {
}
