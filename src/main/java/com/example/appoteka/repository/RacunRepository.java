package com.example.appoteka.repository;

import com.example.appoteka.entity.Racun;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RacunRepository extends JpaRepository<Racun, Long> {
}
