package com.example.appoteka.mapper;

import com.example.appoteka.dto.LijekDto;
import com.example.appoteka.entity.Lijek;
import com.example.appoteka.form.LijekForm;

public interface LijekMapper {

    LijekDto map(Lijek lijek);

    Lijek mapToModel(LijekForm lijekForm);
}
