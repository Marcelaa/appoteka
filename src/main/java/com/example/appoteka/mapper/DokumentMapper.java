package com.example.appoteka.mapper;

import com.example.appoteka.dto.DokumentDto;
import com.example.appoteka.entity.Dokument;

public interface DokumentMapper {

    DokumentDto map(Dokument dokument);
}
