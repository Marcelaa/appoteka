package com.example.appoteka.mapper.impl;

import com.example.appoteka.dto.RacunDto;
import com.example.appoteka.dto.StavkaDokumentaDto;
import com.example.appoteka.entity.NacinPlacanja;
import com.example.appoteka.entity.Racun;
import com.example.appoteka.form.RacunForm;
import com.example.appoteka.mapper.RacunMapper;
import com.example.appoteka.mapper.StavkaDokumentaMapper;
import com.example.appoteka.repository.NacinPlacanjaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RacunMapperImpl implements RacunMapper {

    private final StavkaDokumentaMapper stavkaDokumentaMapper;

    private final NacinPlacanjaRepository nacinPlacanjaRepository;

    public RacunMapperImpl(StavkaDokumentaMapper stavkaDokumentaMapper, NacinPlacanjaRepository nacinPlacanjaRepository) {
        this.stavkaDokumentaMapper = stavkaDokumentaMapper;
        this.nacinPlacanjaRepository = nacinPlacanjaRepository;
    }

    @Override
    public RacunDto mapWithDetails(Racun racun) {
        RacunDto racunDto = new RacunDto();

        racunDto.setIdDokumenta(racun.getIdDokumenta());
        racunDto.setVrijemeRacuna(racun.getVrijemeRacuna());
        racunDto.setNacinPlacanja(racun.getNacinPlacanja().getNazivNacinaPlacanja());

        List<StavkaDokumentaDto> stavkeDokumenta =
                racun
                        .getDokument()
                        .getStavkeDokumenta()
                        .stream().map(stavkaDokumentaMapper::map).collect(Collectors.toList());
        racunDto.setStavkeDokumenta(stavkeDokumenta);

        return racunDto;
    }

    @Override
    public RacunDto map(Racun racun) {
        RacunDto racunDto = new RacunDto();

        racunDto.setIdDokumenta(racun.getIdDokumenta());
        racunDto.setVrijemeRacuna(racun.getVrijemeRacuna());
        racunDto.setNacinPlacanja(racun.getNacinPlacanja().getNazivNacinaPlacanja());

        return racunDto;
    }

    @Override
    public Racun mapToModel(RacunForm racunForm) {
        Racun racun = new Racun();

        racun.setIdDokumenta(racunForm.getIdDokumenta());
        NacinPlacanja nacinPlacanja = nacinPlacanjaRepository.findByNazivNacinaPlacanja(racunForm.getNacinPlacanja());
        racun.setIdNacinaPlacanja(nacinPlacanja.getIdNacinaPlacanja());
        racun.setNacinPlacanja(nacinPlacanja);

        return racun;
    }
}
