package com.example.appoteka.mapper.impl;

import com.example.appoteka.dto.LijekDto;
import com.example.appoteka.dto.ProizvodacDto;
import com.example.appoteka.entity.Lijek;
import com.example.appoteka.form.LijekForm;
import com.example.appoteka.mapper.LijekMapper;
import com.example.appoteka.mapper.ProizvodacMapper;
import com.example.appoteka.repository.ProizvodacRepository;
import org.springframework.stereotype.Component;

@Component
public class LijekMapperImpl implements LijekMapper {

    private final ProizvodacMapper proizvodacMapper;
    private final ProizvodacRepository proizvodacRepository;

    public LijekMapperImpl(ProizvodacMapper proizvodacMapper, ProizvodacRepository proizvodacRepository) {
        this.proizvodacMapper = proizvodacMapper;
        this.proizvodacRepository = proizvodacRepository;
    }

    @Override
    public LijekDto map(Lijek lijek) {
        LijekDto lijekDto = new LijekDto();

        lijekDto.setBezreceptniLijek(lijek.getBezreceptniLijek());
        lijekDto.setCijenaLijeka(lijek.getCijenaLijeka());
        lijekDto.setIdLijeka(lijek.getIdLijeka());
        lijekDto.setImeLijeka(lijek.getImeLijeka());
        lijekDto.setKolicinaNaZalihi(lijek.getKolicinaNaZalihi());

        ProizvodacDto proizvodacDto = proizvodacMapper.map(lijek.getProizvodac());
        lijekDto.setProizvodac(proizvodacDto);

        return lijekDto;
    }

    @Override
    public Lijek mapToModel(LijekForm lijekForm) {
        Lijek lijek = new Lijek();

        lijek.setBezreceptniLijek(lijekForm.getBezreceptniLijek());
        lijek.setCijenaLijeka(lijekForm.getCijenaLijeka());
        lijek.setImeLijeka(lijekForm.getImeLijeka());
        lijek.setIdProizvodaca(proizvodacRepository.findByNazivProizvodaca(lijekForm.getProizvodac()).getIdProizvodaca());
        lijek.setKolicinaNaZalihi(lijekForm.getKolicinaNaZalihi());

        return lijek;
    }
}
