package com.example.appoteka.mapper.impl;

import com.example.appoteka.dto.LijekDto;
import com.example.appoteka.dto.StavkaDokumentaDto;
import com.example.appoteka.entity.StavkaDokumenta;
import com.example.appoteka.mapper.LijekMapper;
import com.example.appoteka.mapper.StavkaDokumentaMapper;
import org.springframework.stereotype.Component;

@Component
public class StavkaDokumentaMapperImpl implements StavkaDokumentaMapper {

    private final LijekMapper lijekMapper;

    public StavkaDokumentaMapperImpl(LijekMapper lijekMapper) {
        this.lijekMapper = lijekMapper;
    }

    @Override
    public StavkaDokumentaDto map(StavkaDokumenta stavkaDokumenta) {
        StavkaDokumentaDto stavkaDokumentaDto = new StavkaDokumentaDto();

        stavkaDokumentaDto.setIdDokumenta(stavkaDokumenta.getIdDokumenta());
        stavkaDokumentaDto.setKolicinaLijekaDokumenta(stavkaDokumenta.getKolicinaLijekaDokumenta());

        LijekDto lijekDto = lijekMapper.map(stavkaDokumenta.getLijek());
        stavkaDokumentaDto.setLijek(lijekDto);

        return stavkaDokumentaDto;
    }
}
