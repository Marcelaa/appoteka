package com.example.appoteka.mapper.impl;

import com.example.appoteka.dto.ProizvodacDto;
import com.example.appoteka.entity.Proizvodac;
import com.example.appoteka.mapper.ProizvodacMapper;
import org.springframework.stereotype.Component;

@Component
public class ProizvodacMapperImpl implements ProizvodacMapper {

    @Override
    public ProizvodacDto map(Proizvodac proizvodac) {
        ProizvodacDto proizvodacDto = new ProizvodacDto();

        proizvodacDto.setEmailProizvodaca(proizvodac.getEmailProizvodaca());
        proizvodacDto.setIdProizvodaca(proizvodac.getIdProizvodaca());
        proizvodacDto.setNazivProizvodaca(proizvodac.getNazivProizvodaca());

        return proizvodacDto;
    }
}
