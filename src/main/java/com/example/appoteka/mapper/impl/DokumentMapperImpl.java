package com.example.appoteka.mapper.impl;

import com.example.appoteka.dto.DokumentDto;
import com.example.appoteka.dto.StavkaDokumentaDto;
import com.example.appoteka.entity.Dokument;
import com.example.appoteka.mapper.DokumentMapper;
import com.example.appoteka.mapper.StavkaDokumentaMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DokumentMapperImpl implements DokumentMapper {

    private final StavkaDokumentaMapper stavkaDokumentaMapper;

    public DokumentMapperImpl(StavkaDokumentaMapper stavkaDokumentaMapper) {
        this.stavkaDokumentaMapper = stavkaDokumentaMapper;
    }

    @Override
    public DokumentDto map(Dokument dokument) {
        DokumentDto dokumentDto = new DokumentDto();

        dokumentDto.setDatumDokumenta(dokument.getDatumDokumenta());
        dokumentDto.setIdDokumenta(dokument.getIdDokumenta());

        List<StavkaDokumentaDto> stavkeDokumenta = dokument.getStavkeDokumenta()
                .stream().map(stavkaDokumentaMapper::map).collect(Collectors.toList());
        dokumentDto.setStavkeDokumenta(stavkeDokumenta);

        return dokumentDto;
    }
}
