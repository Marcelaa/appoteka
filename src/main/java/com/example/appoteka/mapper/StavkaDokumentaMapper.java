package com.example.appoteka.mapper;

import com.example.appoteka.dto.StavkaDokumentaDto;
import com.example.appoteka.entity.StavkaDokumenta;

public interface StavkaDokumentaMapper {

    StavkaDokumentaDto map(StavkaDokumenta stavkaDokumenta);
}
