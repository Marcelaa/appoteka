package com.example.appoteka.mapper;

import com.example.appoteka.dto.ProizvodacDto;
import com.example.appoteka.entity.Proizvodac;

public interface ProizvodacMapper {

    ProizvodacDto map(Proizvodac proizvodac);
}
