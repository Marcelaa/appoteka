package com.example.appoteka.mapper;

import com.example.appoteka.dto.RacunDto;
import com.example.appoteka.entity.Racun;
import com.example.appoteka.form.RacunForm;

public interface RacunMapper {

    RacunDto mapWithDetails(Racun racun);

    RacunDto map(Racun racun);

    Racun mapToModel(RacunForm racunForm);
}
