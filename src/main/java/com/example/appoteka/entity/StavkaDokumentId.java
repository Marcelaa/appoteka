package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StavkaDokumentId implements Serializable {

    private Long idLijeka;
    private Long idDokumenta;

}
