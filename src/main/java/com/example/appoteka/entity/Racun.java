package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RACUN")
public class Racun {

    @Id
    @Column(name = "IDDOKUMENTA")
    private Long idDokumenta;

    @Column(name = "VRIJEMERACUNA")
    private LocalDate vrijemeRacuna;

    @Column(name = "IDNACINAPLACANJA")
    private Long idNacinaPlacanja;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "IDDOKUMENTA", referencedColumnName = "IDDOKUMENTA")
    private Dokument dokument;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="IDNACINAPLACANJA", insertable = false, updatable = false)
    private NacinPlacanja nacinPlacanja;
}
