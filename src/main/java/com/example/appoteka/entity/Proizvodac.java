package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PROIZVODAC")
public class Proizvodac {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDPROIZVODACA")
    private Long idProizvodaca;

    @Column(name = "NAZIVPROIZVODACA")
    private String nazivProizvodaca;

    @Column(name = "EMAILPROIZVODACA")
    private String emailProizvodaca;

    @OneToMany(mappedBy = "proizvodac")
    private List<Lijek> lijekovi;
}
