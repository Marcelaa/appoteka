package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "DOKUMENT")
public class Dokument {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDDOKUMENTA")
    private Long idDokumenta;

    @Column(name = "DATUMDOKUMENTA")
    private LocalDate datumDokumenta;

    @OneToOne(mappedBy = "dokument")
    private Racun racun;

    @OneToMany(mappedBy="dokument")
    private List<StavkaDokumenta> stavkeDokumenta;
}
