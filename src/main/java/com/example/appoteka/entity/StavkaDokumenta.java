package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "STAVKADOKUMENTA")
@IdClass(StavkaDokumentId.class)
public class StavkaDokumenta {

    @Id
    @Column(name = "IDDOKUMENTA")
    private Long idDokumenta;

    @Id
    @Column(name = "IDLIJEKA")
    private Long idLijeka;

    @Column(name = "KOLICINALIJEKADOKUMENTA")
    private Integer kolicinaLijekaDokumenta;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IDDOKUMENTA", insertable = false, updatable = false)
    private Dokument dokument;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IDLIJEKA", insertable = false, updatable = false)
    private Lijek lijek;
}


