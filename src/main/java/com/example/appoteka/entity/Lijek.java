package com.example.appoteka.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "LIJEK")
public class Lijek {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDLIJEKA")
    private Long idLijeka;

    @Column(name = "IMELIJEKA")
    private String imeLijeka;

    @Column(name = "KOLICINANAZALIHI")
    private Integer kolicinaNaZalihi;

    @Column(name = "CIJENALIJEKA")
    private Double cijenaLijeka;

    @Column(name = "BEZRECEPTNILIJEK")
    private String bezreceptniLijek;

    @Column(name = "IDPROIZVODACA")
    private Long idProizvodaca;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "IDPROIZVODACA", insertable = false, updatable = false)
    private Proizvodac proizvodac;
}
