package com.example.appoteka.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StavkaDokumentaForm {

    private Long idDokumenta;
    private String imeLijeka;
    private Integer kolicinaLijekaDokumenta;
}
