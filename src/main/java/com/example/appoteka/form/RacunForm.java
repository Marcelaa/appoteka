package com.example.appoteka.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RacunForm {

    private Long idDokumenta;

    private String nacinPlacanja;
}