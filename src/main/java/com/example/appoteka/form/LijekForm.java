package com.example.appoteka.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LijekForm {

    private String imeLijeka;
    private Integer kolicinaNaZalihi;
    private Double cijenaLijeka;
    private String bezreceptniLijek;
    private String proizvodac;
}
