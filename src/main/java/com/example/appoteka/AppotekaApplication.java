package com.example.appoteka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppotekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppotekaApplication.class, args);
	}

}
