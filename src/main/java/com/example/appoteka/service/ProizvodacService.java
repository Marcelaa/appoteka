package com.example.appoteka.service;

import com.example.appoteka.entity.Proizvodac;

import java.util.List;

public interface ProizvodacService {
    public List<Proizvodac> getAllProizvodaci();
}
