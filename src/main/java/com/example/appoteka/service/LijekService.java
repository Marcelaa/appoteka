package com.example.appoteka.service;

import com.example.appoteka.entity.Lijek;

import java.util.List;

public interface LijekService {
    List<Lijek> getAllLijekovi();

    public Lijek getLijekById(Long lijekId);

    void createLijek(Lijek lijek);

    void editLijek(Lijek lijek);

    void deleteLijekById(Long lijekId);

    Boolean existsById(Long lijekId);

    Boolean existsByImeLijeka(String imeLijeka);

    Lijek getLijekByImeLijeka(String imeLijeka);
}
