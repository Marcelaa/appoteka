package com.example.appoteka.service;

import com.example.appoteka.entity.Racun;

import java.util.List;

public interface RacunService {

    List<Racun> getAllRacuni();

    Racun getRacunByDocumentId(Long dokumentId);

    void createRacun(Racun racun);

    void editRacun(Racun racun);

    void deleteRacunByDocumentId(Long dokumentId);

    Boolean existsByDocumentId(long dokumentId);
}
