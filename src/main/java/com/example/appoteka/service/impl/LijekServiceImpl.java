package com.example.appoteka.service.impl;

import com.example.appoteka.entity.Lijek;
import com.example.appoteka.exception.FetchException;
import com.example.appoteka.repository.LijekRepository;
import com.example.appoteka.service.LijekService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LijekServiceImpl implements LijekService {

    private final LijekRepository lijekRepository;

    public LijekServiceImpl(LijekRepository lijekRepository) {
        this.lijekRepository = lijekRepository;
    }

    @Override
    public List<Lijek> getAllLijekovi() {
        return lijekRepository.findAll();
    }

    @Override
    public Lijek getLijekById(Long lijekId) {
        return Optional.of(lijekRepository.findById(lijekId))
                .orElseThrow(() -> new FetchException("Cannot fetch LIJEK with ID: " + lijekId)).get();
    }

    @Override
    public void createLijek(Lijek lijek) {
        lijekRepository.save(lijek);
    }

    @Override
    public void editLijek(Lijek lijek) {
        lijekRepository.save(lijek);
    }

    @Override
    public void deleteLijekById(Long lijekId) {
        lijekRepository.deleteById(lijekId);
    }

    @Override
    public Boolean existsById(Long lijekId) {
        return lijekRepository.existsById(lijekId);
    }

    @Override
    public Boolean existsByImeLijeka(String imeLijeka) {
        return lijekRepository.existsByImeLijeka(imeLijeka);
    }

    @Override
    public Lijek getLijekByImeLijeka(String imeLijeka) {
        return lijekRepository.findByImeLijeka(imeLijeka);
    }
}
