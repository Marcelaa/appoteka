package com.example.appoteka.service.impl;

import com.example.appoteka.entity.Racun;
import com.example.appoteka.exception.FetchException;
import com.example.appoteka.repository.RacunRepository;
import com.example.appoteka.service.RacunService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RacunServiceImpl implements RacunService {

    private final RacunRepository racunRepository;

    public RacunServiceImpl(RacunRepository racunRepository) {
        this.racunRepository = racunRepository;
    }

    @Override
    public List<Racun> getAllRacuni() {
        return racunRepository.findAll(Sort.by(Sort.Direction.DESC, "vrijemeRacuna"));
    }

    @Override
    public Racun getRacunByDocumentId(Long dokumentId) {
        return Optional.of(racunRepository.findById(dokumentId))
                .orElseThrow(() -> new FetchException("Cannot fetch RACUN with ID: " + dokumentId)).get();
    }

    @Override
    public void createRacun(Racun racun) {
        racunRepository.save(racun);
    }

    @Override
    public void editRacun(Racun racun) {
        racunRepository.save(racun);
    }

    @Override
    public void deleteRacunByDocumentId(Long dokumentId) {
        racunRepository.deleteById(dokumentId);
    }

    @Override
    public Boolean existsByDocumentId(long dokumentId) {
        return racunRepository.existsById(dokumentId);
    }

}
