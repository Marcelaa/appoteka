package com.example.appoteka.service.impl;

import com.example.appoteka.entity.Lijek;
import com.example.appoteka.entity.Proizvodac;
import com.example.appoteka.repository.ProizvodacRepository;
import com.example.appoteka.service.ProizvodacService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProizvodacServiceImpl implements ProizvodacService{

    private final ProizvodacRepository proizvodacRepository;

    public ProizvodacServiceImpl(ProizvodacRepository proizvodacRepository) {
        this.proizvodacRepository = proizvodacRepository;
    }

    @Override
    public List<Proizvodac> getAllProizvodaci() {
        return proizvodacRepository.findAll();
    }

}