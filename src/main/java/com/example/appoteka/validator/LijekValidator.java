package com.example.appoteka.validator;

import com.example.appoteka.entity.StavkaDokumentId;
import com.example.appoteka.form.LijekForm;
import com.example.appoteka.form.StavkaDokumentaForm;

public interface LijekValidator {

    void validateCreate(LijekForm lijekForm);

    void validateEdit(LijekForm lijekForm, Long lijekId);

    void validateLijekId(Long lijekId);

    void validateStavkaDokumenta(StavkaDokumentaForm stavkaDokumentaForm);

    void validateStavkaDokumentaId(StavkaDokumentId stavkaDokumentId);
}
