package com.example.appoteka.validator;

import com.example.appoteka.form.RacunForm;

public interface RacunValidator {
    void validateCreate(RacunForm racunForm);

    void validateEdit(RacunForm racunForm, Long dokumentId);

    void validateDocumentId(Long dokumentId);
}
