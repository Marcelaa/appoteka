package com.example.appoteka.validator.impl;

import com.example.appoteka.entity.StavkaDokumentId;
import com.example.appoteka.form.LijekForm;
import com.example.appoteka.form.StavkaDokumentaForm;
import com.example.appoteka.repository.DokumentRepository;
import com.example.appoteka.repository.ProizvodacRepository;
import com.example.appoteka.service.LijekService;
import com.example.appoteka.validator.LijekValidator;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class LijekValidatorImpl implements LijekValidator {

    private final ProizvodacRepository proizvodacRepository;
    private final LijekService lijekService;
    private final DokumentRepository dokumentRepository;

    public LijekValidatorImpl(ProizvodacRepository proizvodacRepository, LijekService lijekService, DokumentRepository dokumentRepository) {
        this.proizvodacRepository = proizvodacRepository;
        this.lijekService = lijekService;
        this.dokumentRepository = dokumentRepository;
    }


    @Override
    public void validateCreate(LijekForm lijekForm) {

        Assert.notNull(lijekForm.getCijenaLijeka(), "Lijek mora imati svoju cijenu.");
        Assert.isTrue(lijekForm.getCijenaLijeka() > 0, "Cijena lijeka mora biti pozitivan broj.");
        Assert.hasText(lijekForm.getBezreceptniLijek(), "Lijek mora biti popunjeno polje bezreceptniLijek.");
        Assert.isTrue(lijekForm.getBezreceptniLijek().equalsIgnoreCase("da")
                || lijekForm.getBezreceptniLijek().equalsIgnoreCase("ne"), "Polje bezreceptniLijek mora biti da/ne.");
        Assert.hasText(lijekForm.getImeLijeka(), "Lijek mora imati svoje ime.");
        Assert.notNull(lijekForm.getKolicinaNaZalihi(), "Lijek mora imati svoju količinu na zalihi.");
        Assert.isTrue(lijekForm.getKolicinaNaZalihi() >= 0, "Količina lijeka mora biti ili pozitivan broj ili 0.");
        Assert.hasText(lijekForm.getProizvodac(), "Lijek mora imati svog proizvođača.");
        Assert.isTrue(proizvodacRepository.existsByNazivProizvodaca(lijekForm.getProizvodac()), "Proizvođač lijeka mora postojati u bazi podataka.");

    }

    @Override
    public void validateEdit(LijekForm lijekForm, Long lijekId) {

        validateLijekId(lijekId);

        Assert.notNull(lijekForm.getCijenaLijeka(), "Lijek mora imati svoju cijenu.");
        Assert.isTrue(lijekForm.getCijenaLijeka() > 0, "Cijena lijeka mora biti pozitivan broj.");
        Assert.hasText(lijekForm.getBezreceptniLijek(), "Lijek mora biti popunjeno polje bezreceptniLijek.");
        Assert.isTrue(lijekForm.getBezreceptniLijek().equalsIgnoreCase("da")
                || lijekForm.getBezreceptniLijek().equalsIgnoreCase("ne"), "Polje bezreceptniLijek mora biti da/ne.");
        Assert.hasText(lijekForm.getImeLijeka(), "Lijek mora imati svoje ime.");
        Assert.notNull(lijekForm.getKolicinaNaZalihi(), "Lijek mora imati svoju količinu na zalihi.");
        Assert.isTrue(lijekForm.getKolicinaNaZalihi() >= 0, "Količina lijeka mora biti ili pozitivan broj ili 0.");
        Assert.hasText(lijekForm.getProizvodac(), "Lijek mora imati svog proizvođača.");
        Assert.isTrue(proizvodacRepository.existsByNazivProizvodaca(lijekForm.getProizvodac()), "Proizvođač lijeka mora postojati u bazi podataka.");
    }

    @Override
    public void validateLijekId(Long lijekId) {

        Assert.notNull(lijekId, "Lijek mora imati svoj ID");
        Assert.isTrue(lijekService.existsById(lijekId), "Lijek mora već postojati da bi ga se ažuriralo/izbrisalo.");
    }

    @Override
    public void validateStavkaDokumentaId(StavkaDokumentId stavkaDokumentId) {
        Assert.notNull(stavkaDokumentId.getIdDokumenta(), "Lijek mora imati idDokumenta kojem se treba dodati.");
        Assert.isTrue(dokumentRepository.existsById(stavkaDokumentId.getIdDokumenta()), "Dokument mora već postojati u bazi podataka.");
        Assert.notNull(stavkaDokumentId.getIdLijeka(), "Lijek mora imati id.");
        Assert.isTrue(lijekService.existsById(stavkaDokumentId.getIdLijeka()), "Lijek mora već postojati u bazi podataka.");

    }

    @Override
    public void validateStavkaDokumenta(StavkaDokumentaForm stavkaDokumentaForm) {

        Assert.hasText(stavkaDokumentaForm.getImeLijeka(), "Lijek mora imati svoje ime.");
        Assert.isTrue(lijekService.existsByImeLijeka(stavkaDokumentaForm.getImeLijeka()), "Lijek mora postojati u bazi podataka.");
        Assert.notNull(stavkaDokumentaForm.getIdDokumenta(), "Lijek mora imati idDokumenta kojem se treba dodati.");
        Assert.isTrue(dokumentRepository.existsById(stavkaDokumentaForm.getIdDokumenta()), "Dokument mora već postojati u bazi podataka.");
        Assert.notNull(stavkaDokumentaForm.getKolicinaLijekaDokumenta(), "Lijek mora imati svoju količinu.");
        Assert.isTrue(stavkaDokumentaForm.getKolicinaLijekaDokumenta() >= 0, "Količina lijeka mora biti ili pozitivan broj ili 0.");

    }
}
