package com.example.appoteka.validator.impl;

import com.example.appoteka.form.RacunForm;
import com.example.appoteka.repository.DokumentRepository;
import com.example.appoteka.repository.NacinPlacanjaRepository;
import com.example.appoteka.service.RacunService;
import com.example.appoteka.validator.RacunValidator;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class RacunValidatorImpl implements RacunValidator {

    private final RacunService racunService;
    private final DokumentRepository dokumentRepository;
    private final NacinPlacanjaRepository nacinPlacanjaRepository;

    public RacunValidatorImpl(RacunService racunService, DokumentRepository dokumentRepository, NacinPlacanjaRepository nacinPlacanjaRepository) {
        this.racunService = racunService;
        this.dokumentRepository = dokumentRepository;
        this.nacinPlacanjaRepository = nacinPlacanjaRepository;
    }


    @Override
    public void validateCreate(RacunForm racunForm) {

        Assert.notNull(racunForm.getIdDokumenta(), "Račun mora imati svoj dokumentID");
        Assert.hasText(racunForm.getNacinPlacanja(), "Račun mora imati načina plaćanja.");
        Assert.isTrue(nacinPlacanjaRepository.existsByNazivNacinaPlacanja(racunForm.getNacinPlacanja()), "Način plaćanja mora postojati u bazi podataka.");

    }

    @Override
    public void validateEdit(RacunForm racunForm, Long dokumentId) {

        validateDocumentId(dokumentId);

        Assert.hasText(racunForm.getNacinPlacanja(), "Račun mora imati načina plaćanja.");
        Assert.isTrue(nacinPlacanjaRepository.existsByNazivNacinaPlacanja(racunForm.getNacinPlacanja()), "Način plaćanja mora postojati u bazi podataka.");

    }

    @Override
    public void validateDocumentId(Long dokumentId) {

        Assert.notNull(dokumentId, "Račun mora imati svoj dokumentID");
        Assert.isTrue(racunService.existsByDocumentId(dokumentId), "Račun mora već postojati da bi ga se ažuriralo/izbrisalo.");
        Assert.isTrue(dokumentRepository.existsById(dokumentId), "Pripadni dokument mora već postojati da bi se ažuriralo/izbrisalo račun.");

    }
}
