package com.example.appoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProizvodacDto {

    private Long idProizvodaca;
    private String nazivProizvodaca;
    private String emailProizvodaca;
}
