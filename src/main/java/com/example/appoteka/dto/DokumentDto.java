package com.example.appoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DokumentDto {

    private Long idDokumenta;
    private LocalDate datumDokumenta;
    private List<StavkaDokumentaDto> stavkeDokumenta;
}
