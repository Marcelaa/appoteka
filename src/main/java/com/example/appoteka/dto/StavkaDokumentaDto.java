package com.example.appoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StavkaDokumentaDto {

    private Long idDokumenta;
    private LijekDto lijek;
    private Integer kolicinaLijekaDokumenta;
}
