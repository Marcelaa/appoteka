package com.example.appoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LijekDto {

    private Long idLijeka;
    private String imeLijeka;
    private Integer kolicinaNaZalihi;
    private Double cijenaLijeka;
    private String bezreceptniLijek;
    private ProizvodacDto proizvodac;
}
