package com.example.appoteka.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RacunDto {

    private Long idDokumenta;
    private LocalDate vrijemeRacuna;
    private String nacinPlacanja;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<StavkaDokumentaDto> stavkeDokumenta;
}
