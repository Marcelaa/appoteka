package com.example.appoteka.controller;

import com.example.appoteka.dto.RacunDto;
import com.example.appoteka.entity.*;
import com.example.appoteka.form.RacunForm;
import com.example.appoteka.form.StavkaDokumentaForm;
import com.example.appoteka.mapper.RacunMapper;
import com.example.appoteka.repository.DokumentRepository;
import com.example.appoteka.repository.StavkaDokumentaRepository;
import com.example.appoteka.service.LijekService;
import com.example.appoteka.service.RacunService;
import com.example.appoteka.validator.LijekValidator;
import com.example.appoteka.validator.RacunValidator;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/racuni")
public class RacunController {

    private final RacunService racunService;
    private final RacunMapper racunMapper;
    private final RacunValidator racunValidator;
    private final DokumentRepository dokumentRepository;
    private final LijekValidator lijekValidator;
    private LijekService lijekService;

    private final StavkaDokumentaRepository stavkaDokumentaRepository;

    public RacunController(RacunService racunService, RacunMapper racunMapper, RacunValidator racunValidator, DokumentRepository dokumentRepository, LijekValidator lijekValidator, LijekService lijekService, StavkaDokumentaRepository stavkaDokumentaRepository) {
        this.racunService = racunService;
        this.racunMapper = racunMapper;
        this.racunValidator = racunValidator;
        this.dokumentRepository = dokumentRepository;
        this.lijekValidator = lijekValidator;
        this.lijekService = lijekService;
        this.stavkaDokumentaRepository = stavkaDokumentaRepository;
    }

    @GetMapping("/details")
    public List<RacunDto> getAllRacuniWithDetails() {
        return racunService.getAllRacuni()
                .stream()
                .map(racunMapper::mapWithDetails)
                .collect(Collectors.toList());
    }

    @GetMapping
    public List<RacunDto> getAllRacuni() {
        return racunService.getAllRacuni()
                .stream()
                .map(racunMapper::map)
                .collect(Collectors.toList());
    }


    @GetMapping("/details/{dokumentId}")
    public RacunDto getRacunByIdWithDetails(@PathVariable("dokumentId") Long dokumentId) {
        return racunMapper.mapWithDetails(racunService.getRacunByDocumentId(dokumentId));
    }

    @GetMapping("/{dokumentId}")
    public RacunDto getRacunById(@PathVariable("dokumentId") Long dokumentId) {
        return racunMapper.map(racunService.getRacunByDocumentId(dokumentId));
    }

    @PostMapping
    public void createRacun(@RequestBody RacunForm racunForm) {
        racunValidator.validateCreate(racunForm);
        Racun racun = racunMapper.mapToModel(racunForm);
        racun.setVrijemeRacuna(LocalDate.now());

        if (!dokumentRepository.existsById(racun.getIdDokumenta())) {
            Dokument dokument = new Dokument();
            dokument.setIdDokumenta(racun.getIdDokumenta());
            dokument.setDatumDokumenta(racun.getVrijemeRacuna());
            dokument.setStavkeDokumenta(new ArrayList<>());

            dokumentRepository.save(dokument);
        }

        racunService.createRacun(racun);
    }

    @PutMapping("/{dokumentId}")
    public void editRacun(@RequestBody RacunForm racunForm, @PathVariable("dokumentId") Long dokumentId) {
        racunValidator.validateEdit(racunForm, dokumentId);
        Racun racun = racunMapper.mapToModel(racunForm);
        racun.setIdDokumenta(dokumentId);
        racun.setVrijemeRacuna(racunService.getRacunByDocumentId(dokumentId).getVrijemeRacuna());

        racunService.editRacun(racun);
    }

    @DeleteMapping("/{dokumentId}")
    public void deleteRacunByDocumentId(@PathVariable("dokumentId") Long dokumentId) {
        racunValidator.validateDocumentId(dokumentId);

        racunService.deleteRacunByDocumentId(dokumentId);
    }

    @PostMapping("/lijekovi")
    public void addLijekToRacun(@RequestBody StavkaDokumentaForm stavkaDokumentaForm) {
        lijekValidator.validateStavkaDokumenta(stavkaDokumentaForm);

        StavkaDokumenta stavkaDokumenta = new StavkaDokumenta();
        stavkaDokumenta.setIdDokumenta(stavkaDokumentaForm.getIdDokumenta());
        stavkaDokumenta.setKolicinaLijekaDokumenta(stavkaDokumentaForm.getKolicinaLijekaDokumenta());

        Lijek lijek = lijekService.getLijekByImeLijeka(stavkaDokumentaForm.getImeLijeka());
        stavkaDokumenta.setIdLijeka(lijek.getIdLijeka());

        stavkaDokumentaRepository.save(stavkaDokumenta);
    }
}
