package com.example.appoteka.controller;

import com.example.appoteka.dto.ProizvodacDto;
import com.example.appoteka.mapper.ProizvodacMapper;
import com.example.appoteka.service.ProizvodacService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/proizvodaci")
public class ProizvodacController {

    private final ProizvodacService proizvodacService;
    private final ProizvodacMapper proizvodacMapper;

    public ProizvodacController(ProizvodacService proizvodacService, ProizvodacMapper proizvodacMapper) {
        this.proizvodacService = proizvodacService;
        this.proizvodacMapper = proizvodacMapper;
    }

    @GetMapping
    public List<ProizvodacDto> getAllProizvodaci() {
        return proizvodacService.getAllProizvodaci()
                .stream()
                .map(proizvodacMapper::map)
                .collect(Collectors.toList());
    }

}
