package com.example.appoteka.controller;

import com.example.appoteka.dto.LijekDto;
import com.example.appoteka.entity.Lijek;
import com.example.appoteka.form.LijekForm;
import com.example.appoteka.mapper.LijekMapper;
import com.example.appoteka.service.LijekService;
import com.example.appoteka.validator.LijekValidator;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/lijekovi")
public class LijekController {

    private final LijekService lijekService;
    private final LijekMapper lijekMapper;
    private final LijekValidator lijekValidator;

    public LijekController(LijekService lijekService, LijekMapper lijekMapper, LijekValidator lijekValidator) {
        this.lijekService = lijekService;
        this.lijekMapper = lijekMapper;
        this.lijekValidator = lijekValidator;
    }

    @GetMapping
    public List<LijekDto> getAllLijekovi() {
        return lijekService.getAllLijekovi()
                .stream()
                .map(lijekMapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping("/{lijekId}")
    public LijekDto getLijekById(@PathVariable("lijekId") Long lijekId) {
        return lijekMapper.map(lijekService.getLijekById(lijekId));
    }

    @PostMapping
    public void createLijek(@RequestBody LijekForm lijekForm) {
        lijekValidator.validateCreate(lijekForm);

        Lijek lijek = lijekMapper.mapToModel(lijekForm);

        lijekService.createLijek(lijek);
    }

    @PutMapping("/{lijekId}")
    public void editLijek(@RequestBody LijekForm lijekForm, @PathVariable("lijekId") Long lijekId) {
        lijekValidator.validateEdit(lijekForm, lijekId);

        Lijek lijek = lijekMapper.mapToModel(lijekForm);
        lijek.setIdLijeka(lijekId);

        lijekService.editLijek(lijek);
    }

    @DeleteMapping("/{lijekId}")
    public void deleteLijekById(@PathVariable("lijekId") Long lijekId) {
        lijekValidator.validateLijekId(lijekId);

        lijekService.deleteLijekById(lijekId);
    }
}
